# LayoutManagerGroup

 LayoutManagerGroup : LayoutManagerGroup is a custom set of views that responsible for measuring and positioning the item views within a
 RecyclerView as well as determining the policy for when to recycle item views that are no longer visible to the user.
 This lib allows arrange views like echelon and a special skid right in aligned manner.

# LayoutManagerGroup includes :
 
* The following core classes are the essential interface to create the layoutmanager:
    RecyclerView(custom component): The entry point for creating the user interface with the help of layoutmanagergroup.
    EchelonLayoutManager: Manages and create the user interface layout.

# Usage Instructions

 1. The steps to initialize the EchelonLayoutManager and the core component recyclerview is below.

    private DirectionalLayout root;
    recyclerView = new RecyclerView(this);
    root.addComponent(recyclerView,
            new ComponentContainer.LayoutConfig(HarmonyConstant.getMaximumWindowMetrics(this).getWidth(),
                    HarmonyConstant.getMaximumWindowMetrics(this).getHeight()));

	EchelonLayoutManager echelonLayoutManager = new EchelonLayoutManager(this);
    recyclerView.setLayoutManager(echelonLayoutManager);
    AdapterOriginal adapterOriginal = new AdapterOriginal(false);
    recyclerView.setAdapter(adapterOriginal);

 2. The steps to initialize the SkidRightLayoutManager and the core component recyclerview is below.

    SkidRightLayoutManager skidRightLayoutManager = new SkidRightLayoutManager(/* Pass item height width ratio */, /* Pass scale */);
    recyclerView.setLayoutManager(skidRightLayoutManager);
    SkidRightAdapter skidRightAdapter = new SkidRightAdapter();
    recyclerView.setAdapter(skidRightAdapter);

# Installation Instructions

    1.For using LayoutManagerGroup module in a sample application, include the below library dependency to generate hap/layoutmanagergroup.har.

    Modify entry build.gradle as below :
    
    dependencies {
        implementation project(path ':layoutmanagergroup’)
    }

    2.For using LayoutManagerGroup in a separate application, make sure to add the "layoutmanagergroup.har" in libs folder of "entry" module.

    Modify entry build.gradle as below :
    
    dependencies {
        implementation fileTree(dir: 'libs', include: ['*.har'])
    }

    3.For using LayoutManagerGroup from the remote repository, add the below dependency in "entry" build.gradle.

    Modify entry build.gradle as below :

    allprojects {
       repositories {
         mavenCentral()
       }
    }
    
    dependencies {
        implementation 'io.openharmony.tpc.thirdlib:LayoutManagerGroup:1.0.3'
    }
 
 # License

 Copyright (C) 2018 DingMouRen

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License

