package com.dingmouren.layoutmanagergroup.skidright;

import com.ohos.RecyclerContainer;

import com.dingmouren.layoutmanagergroup.echelon.ItemViewInfo;

import ohos.agp.components.Component;

import java.util.ArrayList;

/**
 * Created by 钉某人
 * github: https://github.com/DingMouRen
 * email: naildingmouren@gmail.com
 */
public class SkidRightLayoutManager extends RecyclerContainer.LayoutManager {
    private boolean mHasChild = false;
    private int mItemViewWidth;
    private int mItemViewHeight;
    private int mScrollOffset = Integer.MAX_VALUE;
    private final float mItemHeightWidthRatio;
    private final float mScale;
    private int mItemCount;
    private final SkidRightSnapHelper mSkidRightSnapHelper;
    private boolean isReverseDirection = false;
    private boolean isSmallSize = false;

    public SkidRightLayoutManager(float itemHeightWidthRatio, float scale) {
        this.mItemHeightWidthRatio = itemHeightWidthRatio;
        this.mScale = scale;
        mSkidRightSnapHelper = new SkidRightSnapHelper();
    }

    @Override
    public RecyclerContainer.LayoutParams generateDefaultLayoutParams() {
        return new RecyclerContainer.LayoutParams(RecyclerContainer.LayoutParams.MATCH_CONTENT,
                RecyclerContainer.LayoutParams.MATCH_CONTENT);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /**
     * getFixedScrollPosition
     *
     * @param direction int
     * @param fixValue  float
     * @return pos int
     */
    public int getFixedScrollPosition(int direction, float fixValue) {
        if (mHasChild) {
            if (mScrollOffset % mItemViewWidth == 0) {
                return RecyclerContainer.NO_POSITION;
            }
            float itemPosition = position();
            int layoutPosition = (int) (direction > 0 ? itemPosition + fixValue : itemPosition + (1 - fixValue)) - 1;
            return convert2AdapterPosition(layoutPosition);
        }
        return RecyclerContainer.NO_POSITION;
    }

    @Override
    public void layoutChildren(RecyclerContainer.Adapter adapter, RecyclerContainer.Recycler recycler) {
        recycler.scrapAllViewsAttached();
        if (!mHasChild) {
            mItemViewHeight = getVerticalSpace();
            mItemViewWidth = (int) (mItemViewHeight / mItemHeightWidthRatio);
            mHasChild = true;
        }
        mItemCount = getRecyclerView().getAdapter().getItemCount();
        mScrollOffset = makeScrollOffsetWithinRange(mScrollOffset);
        fill(recycler);
    }

    private float position() {
        return isReverseDirection ?
                (mScrollOffset + mItemCount * mItemViewWidth) * 1.0F / mItemViewWidth :
                mScrollOffset * 1.0F / mItemViewWidth;
    }

    private void fill(RecyclerContainer.Recycler recycler) {
        int bottomItemPosition = (int) Math.floor(position());
        final int space = getHorizontalSpace();
        final int bottomItemVisibleSize = isReverseDirection ?
                ((mItemCount - 1) * mItemViewWidth + mScrollOffset) % mItemViewWidth :
                mScrollOffset % mItemViewWidth;
        final float offsetPercent = bottomItemVisibleSize * 1.0f / mItemViewWidth;

        int remainSpace = isReverseDirection ? 0 : space - mItemViewWidth;

        final int baseOffsetSpace = isReverseDirection ?
                mItemViewWidth :
                getHorizontalSpace() - mItemViewWidth;

        ArrayList<ItemViewInfo> layoutInfos = new ArrayList<>();
        for (int ival = bottomItemPosition - 1, j = 1;
             ival >= 0; ival--, j++) {
            double maxOffset = baseOffsetSpace / 2 * Math.pow(mScale, j);

            float adjustedPercent = isReverseDirection ? -offsetPercent : +offsetPercent;
            int start = (int) (remainSpace - adjustedPercent * maxOffset);

            float scaleXY = (float) (Math.pow(mScale, j - 1) * (1 - offsetPercent * (1 - mScale)));
            float percent = start * 1.0f / space;
            ItemViewInfo info = new ItemViewInfo(start, scaleXY, offsetPercent, percent);

            layoutInfos.add(0, info);

            double delta = isReverseDirection ? maxOffset : -maxOffset;
            remainSpace += delta;

            boolean isOutOfSpace = isReverseDirection ?
                    remainSpace > getHorizontalSpace() :
                    remainSpace <= 0;
            if (isOutOfSpace) {
                info.setTop((int) (remainSpace - delta));
                info.setPositionOffset(0);
                info.setLayoutPercent(info.getTop() / space);
                info.setScaleXY((float) Math.pow(mScale, j - 1));
                break;
            }
        }

        if (bottomItemPosition < mItemCount) {
            final int start = isReverseDirection ?
                    bottomItemVisibleSize - mItemViewWidth :
                    space - bottomItemVisibleSize;
        } else {
            bottomItemPosition -= 1;
        }

        int layoutCount = layoutInfos.size();
        final int startPos = bottomItemPosition - layoutCount;
        final int endPos = bottomItemPosition - 1;

        final int childCount = getRecyclerView().getChildCount();
        for (int iVal = childCount - 1; iVal >= 0; iVal--) {
            RecyclerContainer.ViewHolder viewHolder = getRecyclerView().findViewHolderForPosition(iVal);
            Component childView = getRecyclerView().getChildAt(iVal);
            if (viewHolder != null) {
                int pos = viewHolder.getPosition();

                if (pos > endPos || pos < startPos) {
                    recycler.detachAndScrapView(childView);
                }
            }
            break;
        }
        recycler.detachDirtyScrapViews();
        for (int i = 0; i < layoutCount; i++) {
            int position = convert2AdapterPosition(startPos + i);
            fillChild(recycler.getViewForPosition(position), layoutInfos.get(i));
        }
    }

    private void fillChild(Component view, ItemViewInfo layoutInfo) {
        addView(view);
        measureChildWithExactlySize(view);
        final int scaleFix = (int) (mItemViewWidth * (1 - layoutInfo.getScaleXY()) / 2);
        int left = layoutInfo.getTop() - scaleFix;
        int top = getRecyclerView().getPaddingTop();
        int right = layoutInfo.getTop() + mItemViewWidth - scaleFix;
        int bottom = top + mItemViewHeight;
        getRecyclerView().layoutDecoratedWithMargins(view, left, top, right, bottom);
        view.setWidth(mItemViewWidth - 1);
        view.setScaleX(layoutInfo.getScaleXY());
        view.setScaleY(layoutInfo.getScaleXY());
    }

    private void measureChildWithExactlySize(Component child) {
        RecyclerContainer.LayoutParams lp = null;
        if (child.getLayoutConfig() instanceof RecyclerContainer.LayoutParams) {
            lp = (RecyclerContainer.LayoutParams) child.getLayoutConfig();
        }
        final int widthSpec = Component.EstimateSpec.getSizeWithMode(mItemViewWidth - child.getMarginLeft() - child.getMarginRight(), Component.EstimateSpec.PRECISE);
        final int heightSpec = Component.EstimateSpec.getSizeWithMode(mItemViewHeight - child.getMarginTop() - child.getMarginBottom(), Component.EstimateSpec.PRECISE);
        child.setWidth(widthSpec);
        child.setHeight(heightSpec);
    }

    private int makeScrollOffsetWithinRange(int scrollOffset) {
        if (isReverseDirection) {
            return Math.max(Math.min(0, scrollOffset), -(mItemCount - 1) * mItemViewWidth);
        } else {
            return Math.min(Math.max(mItemViewWidth, scrollOffset), mItemCount * mItemViewWidth);
        }
    }

    @Override
    public int scrollHorizontallyBy(int dx, RecyclerContainer.Adapter adapter, RecyclerContainer.Recycler recycler) {
        int delta = isReverseDirection ? -dx : dx;
        int pendingScrollOffset = mScrollOffset + delta;
        mScrollOffset = makeScrollOffsetWithinRange(pendingScrollOffset);
        fill(recycler);
        return mScrollOffset - pendingScrollOffset + delta;
    }

    /**
     * calculateDistanceToPosition
     *
     * @param targetPos int
     * @return pos int
     */
    public int calculateDistanceToPosition(int targetPos) {
        if (isReverseDirection) {
            return mItemViewWidth * targetPos + mScrollOffset;
        }
        int pendingScrollOffset = mItemViewWidth * (convert2LayoutPosition(targetPos) + 1);
        return pendingScrollOffset - mScrollOffset;
    }

    @Override
    public boolean canScrollHorizontally() {
        return true;
    }

    private int convert2AdapterPosition(int layoutPosition) {
        return mItemCount - 1 - layoutPosition;
    }

    private int convert2LayoutPosition(int adapterPostion) {
        return mItemCount - 1 - adapterPostion;
    }

    private int getVerticalSpace() {
        return getRecyclerView().getHeight() - getRecyclerView().getPaddingTop()
                - getRecyclerView().getPaddingBottom();
    }

    private int getHorizontalSpace() {
        return getRecyclerView().getWidth() - getRecyclerView().getPaddingLeft() - getRecyclerView().getPaddingRight();
    }
}