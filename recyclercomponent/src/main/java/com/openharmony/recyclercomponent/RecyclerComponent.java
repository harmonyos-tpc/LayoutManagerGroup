/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.openharmony.recyclercomponent;

import ohos.agp.components.*;
import ohos.agp.render.Canvas;
import ohos.app.Context;

import java.util.ArrayList;

/**
 * RecyclerComponent
 */
public class RecyclerComponent extends PositionLayout implements Component.DrawTask, Component.DraggedListener {
    /**
     * recyclerLayoutManager
     */
    public RecyclerLayoutManager recyclerLayoutManager;

    /**
     * recyclerComponentAdapter
     */
    public RecyclerComponentAdapter recyclerComponentAdapter;

    boolean isEmptyCanvas = true;

    private ArrayList<RecyclerComponentHolder> _scrap = new ArrayList<>();

    private ArrayList<RecyclerComponentHolder> _cache = new ArrayList<>();

    /**
     * constructor
     */
    public RecyclerComponent(Context context) {
        super(context);
        setConfig();
    }

    /**
     * constructor
     */
    public RecyclerComponent(Context context, AttrSet attrSet) {
        super(context, attrSet);
        setConfig();
    }

    /**
     * constructor
     */
    public RecyclerComponent(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        setConfig();
    }

    private void setConfig() {
        setClickable(true);
        this.addDrawTask(this);
        this.setDraggedListener(1, this);
    }

    /**
     * onFinishDrawTask
     *
     * @param component Component
     */
    public void onFinishDrawTask(Component component) {
        if (isEmptyCanvas) {
            if (recyclerLayoutManager != null) recyclerLayoutManager.onLayoutChildren();
            isEmptyCanvas = false;
        }
    }

    /**
     * setRecyclerLayoutManager
     *
     * @param layoutManager RecyclerLayoutManager
     */
    public void setRecyclerLayoutManager(RecyclerLayoutManager layoutManager) {
        layoutManager.recyclerComponent = this;
        this.recyclerLayoutManager = layoutManager;
    }

    /**
     * getRecyclerLayoutManager
     *
     * @return RecyclerLayoutManager
     */
    public RecyclerLayoutManager getRecyclerLayoutManager() {
        return recyclerLayoutManager;
    }

    /**
     * setRecyclerComponentAdapter
     *
     * @param recyclerComponentAdapter RecyclerComponentAdapter
     */
    public void setRecyclerComponentAdapter(RecyclerComponentAdapter recyclerComponentAdapter) {
        this.recyclerComponentAdapter = recyclerComponentAdapter;
    }

    /**
     * getRecyclerComponentAdapter
     *
     * @return RecyclerComponentAdapter
     */
    public RecyclerComponentAdapter getRecyclerComponentAdapter() {
        return recyclerComponentAdapter;
    }

    /**
     * notifyDataSetChanged
     */
    public void notifyDataSetChanged() {
        recyclerLayoutManager.onUpdateChildren();
    }

    /**
     * scrap
     *
     * @return int
     */
    public int scrap() {
        int i;
        for (i = 0; i < _cache.size(); i++) {
            if (_cache.get(i).isScrap()) {
                _scrap.add(_cache.get(i));
                removeComponent(_cache.get(i).component);
                _cache.remove(i);
            }
        }
        return i;
    }

    /**
     * recycle
     *
     * @param position int
     * @return RecyclerComponentHolder
     */
    public RecyclerComponentHolder recycle(int position) {
        scrap();
        RecyclerComponentHolder recyclerComponentHolder = null;
        if ((position < recyclerComponentAdapter.getSize()) && (position >= 0)) {
            for (int i = 0; i < _scrap.size(); i++) {
                recyclerComponentHolder = _scrap.get(i);
                if (recyclerComponentAdapter.getTheme(position) == recyclerComponentHolder.theme) {
                    _scrap.remove(recyclerComponentHolder);
                    recyclerComponentHolder.isScrap = false;
                    recyclerComponentAdapter.onBind(position, recyclerComponentHolder);
                    _cache.add(recyclerComponentHolder);
                    recyclerComponentHolder.adapterPosition = position;
                    return recyclerComponentHolder;
                }
            }
            {
                recyclerComponentHolder = recyclerComponentAdapter.onCreate(getContext(),
                        recyclerComponentAdapter.getTheme(position));
                recyclerComponentHolder.inflate(getContext());
                recyclerComponentAdapter.onBind(position, recyclerComponentHolder);
                recyclerComponentHolder.adapterPosition = position;
                recyclerComponentHolder.theme = recyclerComponentAdapter.getTheme(position);
                _cache.add(recyclerComponentHolder);
                return recyclerComponentHolder;
            }
        }
        return null;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        onFinishDrawTask(component);
    }

    @Override
    public void onDragDown(Component component, DragInfo dragInfo) {
        recyclerLayoutManager.onMove(dragInfo, 0);
    }

    @Override
    public void onDragStart(Component component, DragInfo dragInfo) {
        recyclerLayoutManager.onMove(dragInfo, 1);
    }

    @Override
    public void onDragUpdate(Component component, DragInfo dragInfo) {
        recyclerLayoutManager.onMove(dragInfo, 2);
    }

    @Override
    public void onDragEnd(Component component, DragInfo dragInfo) {
        recyclerLayoutManager.onMove(dragInfo, 3);
    }

    @Override
    public void onDragCancel(Component component, DragInfo dragInfo) {
        recyclerLayoutManager.onMove(dragInfo, -1);
    }

    /**
     * RecyclerLayoutManager
     */
    public static abstract class RecyclerLayoutManager {
        /**
         * RecyclerLayoutManager
         */
        public RecyclerComponent recyclerComponent;

        /**
         * onLayoutChildren
         */
        public abstract void onLayoutChildren();

        /**
         * onUpdateChildren
         */
        public abstract void onUpdateChildren();

        /**
         * onMove
         *
         * @param dragInfo DragInfo
         * @param action   int
         */
        public abstract void onMove(DragInfo dragInfo, int action);

        /**
         * getAllComponentHolders
         *
         * @return ArrayList<RecyclerComponentHolder>
         */
        public ArrayList<RecyclerComponentHolder> getAllComponentHolders() {
            return recyclerComponent._cache;
        }
    }

    /**
     * RecyclerComponentAdapter
     */
    public static abstract class RecyclerComponentAdapter {
        /**
         * onCreate
         *
         * @param context Context
         * @param theme   int
         * @return RecyclerComponentHolder
         */
        public abstract RecyclerComponentHolder onCreate(Context context, int theme);

        /**
         * getTheme
         *
         * @param position int
         * @return int
         */
        public abstract int getTheme(int position);

        /**
         * onBind
         *
         * @param position                int
         * @param recyclerComponentHolder RecyclerComponentHolder
         */
        public abstract void onBind(int position, RecyclerComponentHolder recyclerComponentHolder);

        /**
         * getSize
         *
         * @return int
         */
        public abstract int getSize();
    }

    /**
     * RecyclerComponentHolder
     */
    public static abstract class RecyclerComponentHolder {
        /**
         * component
         */
        public Component component = null;

        private int adapterPosition = -1, holderPosition;

        private int theme = -1;

        private boolean isScrap = false;

        private boolean isScrap() {
            return isScrap;
        }

        private void inflate(Context context) {
            component = getComponent(context);
        }

        /**
         * getComponent
         *
         * @param context Context
         * @return Component
         */
        public abstract Component getComponent(Context context);

        /**
         * markScrap
         */
        public void markScrap() {
            isScrap = true;
        }

        /**
         * setHolderPosition
         *
         * @param hp int
         */
        public void setHolderPosition(int hp) {
            holderPosition = hp;
        }

        /**
         * getHolderPosition
         *
         * @return int
         */
        public int getHolderPosition() {
            return holderPosition;
        }

        /**
         * setAdapterPosition
         *
         * @param adapterPosition int
         */
        public void setAdapterPosition(int adapterPosition) {
            this.adapterPosition = adapterPosition;
        }

        /**
         * getAdapterPosition
         *
         * @return int
         */
        public int getAdapterPosition() {
            return adapterPosition;
        }
    }
}
