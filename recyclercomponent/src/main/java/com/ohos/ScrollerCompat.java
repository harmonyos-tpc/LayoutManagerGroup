/*
 * Copyright (C) 2006 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos;

import ohos.app.Context;

import static com.openharmony.recyclercomponent.HarmonyConstant.SDK_INT;


/**
 * ScrollerCompat
 */
public class ScrollerCompat {
    static final int CHASE_FRAME_TIME = 16;

    private static final String TAG = "ScrollerCompat";

    ScrollerCompatImpl mImpl;

    Object mScroller;

    interface ScrollerCompatImpl {
        void abortAnimation(Object obj);

        boolean computeScrollOffset(Object obj);

        Object createScroller(Context context, Interpolator interpolator);

        void fling(Object obj, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8);

        void fling(Object obj, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10);

        float getCurrVelocity(Object obj);

        int getCurrX(Object obj);

        int getCurrY(Object obj);

        int getFinalX(Object obj);

        int getFinalY(Object obj);

        boolean isFinished(Object obj);

        boolean isOverScrolled(Object obj);

        void notifyHorizontalEdgeReached(Object obj, int i, int i2, int i3);

        void notifyVerticalEdgeReached(Object obj, int i, int i2, int i3);

        void startScroll(Object obj, int i, int i2, int i3, int i4);

        void startScroll(Object obj, int i, int i2, int i3, int i4, int i5);
    }

    /**
     * ScrollerCompatImplBase
     */
    static class ScrollerCompatImplBase implements ScrollerCompatImpl {
        /**
         * constructor
         */
        ScrollerCompatImplBase() {
        }

        /**
         * createScroller
         *
         * @param context      Context
         * @param interpolator Interpolator
         * @return Object
         */
        public Object createScroller(Context context, Interpolator interpolator) {
            return interpolator != null ? new Scroller(context, interpolator) : new Scroller(context);
        }

        /**
         * isFinished
         *
         * @param obj Object
         * @return boolean
         */
        public boolean isFinished(Object obj) {
            return ((Scroller) obj).isFinished();
        }

        /**
         * getCurrX
         *
         * @param obj Object
         * @return int
         */
        public int getCurrX(Object obj) {
            return ((Scroller) obj).getCurrX();
        }

        /**
         * getCurrY
         *
         * @param obj Object
         * @return int
         */
        public int getCurrY(Object obj) {
            return ((Scroller) obj).getCurrY();
        }

        /**
         * getCurrVelocity
         *
         * @param obj Object
         * @return float
         */
        public float getCurrVelocity(Object obj) {
            return 0.0f;
        }

        /**
         * computeScrollOffset
         *
         * @param obj Object
         * @return boolean
         */
        public boolean computeScrollOffset(Object obj) {
            return ((Scroller) obj).computeScrollOffset();
        }

        /**
         * startScroll
         *
         * @param obj Object
         * @param i   int
         * @param i2  int
         * @param i3  int
         * @param i4  int
         */
        public void startScroll(Object obj, int i, int i2, int i3, int i4) {
            ((Scroller) obj).startScroll(i, i2, i3, i4);
        }

        /**
         * startScroll
         *
         * @param obj Object
         * @param i   int
         * @param i2  int
         * @param i3  int
         * @param i4  int
         * @param i5  int
         */
        public void startScroll(Object obj, int i, int i2, int i3, int i4, int i5) {
            ((Scroller) obj).startScroll(i, i2, i3, i4, i5);
        }

        /**
         * fling
         *
         * @param obj Object
         * @param i   int
         * @param i2  int
         * @param i3  int
         * @param i4  int
         * @param i5  int
         * @param i6  int
         * @param i7  int
         * @param i8  int
         */
        public void fling(Object obj, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
            ((Scroller) obj).fling(i, i2, i3, i4, i5, i6, i7, i8);
        }

        /**
         * fling
         *
         * @param obj Object
         * @param i   int
         * @param i2  int
         * @param i3  int
         * @param i4  int
         * @param i5  int
         * @param i6  int
         * @param i7  int
         * @param i8  int
         * @param i9  int
         * @param i10 int
         */
        public void fling(Object obj, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9,
                          int i10) {
            ((Scroller) obj).fling(i, i2, i3, i4, i5, i6, i7, i8);
        }

        /**
         * abortAnimation
         *
         * @param obj Object
         */
        public void abortAnimation(Object obj) {
            ((Scroller) obj).abortAnimation();
        }

        /**
         * notifyHorizontalEdgeReached
         *
         * @param obj Object
         * @param i   int
         * @param i2  int
         * @param i3  int
         */
        public void notifyHorizontalEdgeReached(Object obj, int i, int i2, int i3) {
        }

        /**
         * notifyVerticalEdgeReached
         *
         * @param obj Object
         * @param i   int
         * @param i2  int
         * @param i3  int
         */
        public void notifyVerticalEdgeReached(Object obj, int i, int i2, int i3) {
        }

        /**
         * isOverScrolled
         *
         * @param obj Object
         * @return boolean
         */
        public boolean isOverScrolled(Object obj) {
            return false;
        }

        /**
         * getFinalX
         *
         * @param obj Object
         * @return int
         */
        public int getFinalX(Object obj) {
            return ((Scroller) obj).getFinalX();
        }

        /**
         * getFinalY
         *
         * @param obj Object
         * @return int
         */
        public int getFinalY(Object obj) {
            return ((Scroller) obj).getFinalY();
        }
    }

    /**
     * ScrollerCompatImplGingerbread
     */
    static class ScrollerCompatImplGingerbread implements ScrollerCompatImpl {
        /**
         * ScrollerCompatImplGingerbread
         */
        ScrollerCompatImplGingerbread() {
        }

        /**
         * createScroller
         *
         * @param context      Context
         * @param interpolator Interpolator
         * @return Object
         */
        public Object createScroller(Context context, Interpolator interpolator) {
            return null;
        }

        /**
         * isFinished
         *
         * @param obj Object
         * @return boolean
         */
        public boolean isFinished(Object obj) {
            return false;
        }

        /**
         * getCurrX
         *
         * @param obj Object
         * @return int
         */
        public int getCurrX(Object obj) {
            return 0;
        }

        /**
         * getCurrY
         *
         * @param obj Object
         * @return int
         */
        public int getCurrY(Object obj) {
            return 0;
        }

        /**
         * getCurrVelocity
         *
         * @param obj Object
         * @return float
         */
        public float getCurrVelocity(Object obj) {
            return 0.0f;
        }

        /**
         * computeScrollOffset
         *
         * @param obj Object
         * @return boolean
         */
        public boolean computeScrollOffset(Object obj) {
            return false;
        }

        /**
         * startScroll
         *
         * @param obj Object
         * @param i   int
         * @param i2  int
         * @param i3  int
         * @param i4  int
         */
        public void startScroll(Object obj, int i, int i2, int i3, int i4) {
        }


        /**
         * startScroll
         *
         * @param obj Object
         * @param i   int
         * @param i2  int
         * @param i3  int
         * @param i4  int
         * @param i5  int
         */
        public void startScroll(Object obj, int i, int i2, int i3, int i4, int i5) {
        }


        /**
         * fling
         *
         * @param obj Object
         * @param i   int
         * @param i2  int
         * @param i3  int
         * @param i4  int
         * @param i5  int
         * @param i6  int
         * @param i7  int
         * @param i8  int
         */
        public void fling(Object obj, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        }

        /**
         * fling
         *
         * @param obj Object
         * @param i   int
         * @param i2  int
         * @param i3  int
         * @param i4  int
         * @param i5  int
         * @param i6  int
         * @param i7  int
         * @param i8  int
         * @param i9  int
         * @param i10 int
         */
        public void fling(Object obj, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9,
                          int i10) {
        }

        /**
         * abortAnimation
         *
         * @param obj Object
         */
        public void abortAnimation(Object obj) {
        }

        /**
         * notifyHorizontalEdgeReached
         *
         * @param obj Object
         * @param i   int
         * @param i2  int
         * @param i3  int
         */
        public void notifyHorizontalEdgeReached(Object obj, int i, int i2, int i3) {
        }

        /**
         * notifyVerticalEdgeReached
         *
         * @param obj Object
         * @param i   int
         * @param i2  int
         * @param i3  int
         */
        public void notifyVerticalEdgeReached(Object obj, int i, int i2, int i3) {
        }

        /**
         * isOverScrolled
         *
         * @param obj Object
         * @return boolean
         */
        public boolean isOverScrolled(Object obj) {
            return false;
        }

        /**
         * getFinalX
         *
         * @param obj Object
         * @return int
         */
        public int getFinalX(Object obj) {
            return 0;
        }

        /**
         * getFinalY
         *
         * @param obj Object
         * @return int
         */
        public int getFinalY(Object obj) {
            return 0;
        }
    }

    /**
     * ScrollerCompatImplIcs
     */
    static class ScrollerCompatImplIcs extends ScrollerCompatImplGingerbread {
        /**
         * constructor
         */
        ScrollerCompatImplIcs() {
        }

        /**
         * getCurrVelocity
         *
         * @param obj Object
         * @return float
         */
        public float getCurrVelocity(Object obj) {
            return 0;
        }
    }

    /**
     * create
     *
     * @param context Context
     * @return ScrollerCompat
     */
    public static ScrollerCompat create(Context context) {
        return create(context, null);
    }

    /**
     * create
     *
     * @param context      Context
     * @param interpolator Interpolator
     * @return ScrollerCompat
     */
    public static ScrollerCompat create(Context context, Interpolator interpolator) {
        return new ScrollerCompat(context, interpolator);
    }

    /**
     * constructor
     *
     * @param context      Context
     * @param interpolator Interpolator
     */
    ScrollerCompat(Context context, Interpolator interpolator) {
        this(SDK_INT, context, interpolator);
    }

    private ScrollerCompat(int i, Context context, Interpolator interpolator) {
        if (i >= 14) {
            this.mImpl = new ScrollerCompatImplIcs();
        } else if (i >= 9) {
            this.mImpl = new ScrollerCompatImplGingerbread();
        } else {
            this.mImpl = new ScrollerCompatImplBase();
        }
        this.mScroller = this.mImpl.createScroller(context, interpolator);
    }

    public boolean isFinished() {
        return this.mImpl.isFinished(this.mScroller);
    }

    public int getCurrX() {
        return this.mImpl.getCurrX(this.mScroller);
    }

    public int getCurrY() {
        return this.mImpl.getCurrY(this.mScroller);
    }

    public int getFinalX() {
        return this.mImpl.getFinalX(this.mScroller);
    }

    public int getFinalY() {
        return this.mImpl.getFinalY(this.mScroller);
    }

    public float getCurrVelocity() {
        return this.mImpl.getCurrVelocity(this.mScroller);
    }

    public boolean computeScrollOffset() {
        return this.mImpl.computeScrollOffset(this.mScroller);
    }

    public void startScroll(int i, int i2, int i3, int i4) {
        this.mImpl.startScroll(this.mScroller, i, i2, i3, i4);
    }

    public void startScroll(int i, int i2, int i3, int i4, int i5) {
        this.mImpl.startScroll(this.mScroller, i, i2, i3, i4, i5);
    }

    public void fling(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8) {
        this.mImpl.fling(this.mScroller, i, i2, i3, i4, i5, i6, i7, i8);
    }

    public void fling(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10) {
        this.mImpl.fling(this.mScroller, i, i2, i3, i4, i5, i6, i7, i8, i9, i10);
    }

    public void abortAnimation() {
        this.mImpl.abortAnimation(this.mScroller);
    }

    public void notifyHorizontalEdgeReached(int i, int i2, int i3) {
        this.mImpl.notifyHorizontalEdgeReached(this.mScroller, i, i2, i3);
    }

    public void notifyVerticalEdgeReached(int i, int i2, int i3) {
        this.mImpl.notifyVerticalEdgeReached(this.mScroller, i, i2, i3);
    }

    public boolean isOverScrolled() {
        return this.mImpl.isOverScrolled(this.mScroller);
    }
}