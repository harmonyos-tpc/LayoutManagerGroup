/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos;

import ohos.agp.render.Canvas;

import ohos.app.Context;

/**
 * Stub implementation that contains a real EdgeEffect on ICS.
 * <p>
 * This class is an implementation detail for EdgeEffectCompat
 * and should not be used directly.
 */
class EdgeEffectCompatIcs {
    /**
     * newEdgeEffect
     *
     * @param context Context
     * @return Object
     */
    public static Object newEdgeEffect(Context context) {
        return new EdgeEffect(context);
    }

    /**
     * setSize
     *
     * @param edgeEffect Object
     * @param width      int
     * @param height     int
     */
    public static void setSize(Object edgeEffect, int width, int height) {
        ((EdgeEffect) edgeEffect).setSize(width, height);
    }

    /**
     * isFinished
     *
     * @param edgeEffect Object
     * @return boolean
     */
    public static boolean isFinished(Object edgeEffect) {
        return ((EdgeEffect) edgeEffect).isFinished();
    }

    /**
     * finish
     *
     * @param edgeEffect Object
     */
    public static void finish(Object edgeEffect) {
        ((EdgeEffect) edgeEffect).finish();
    }

    /**
     * onPull
     *
     * @param edgeEffect    Object
     * @param deltaDistance float
     * @return boolean
     */
    public static boolean onPull(Object edgeEffect, float deltaDistance) {
        ((EdgeEffect) edgeEffect).onPull(deltaDistance);
        return true;
    }

    /**
     * onRelease
     *
     * @param edgeEffect Object
     * @return boolean
     */
    public static boolean onRelease(Object edgeEffect) {
        EdgeEffect eff = (EdgeEffect) edgeEffect;
        eff.onRelease();
        return eff.isFinished();
    }

    /**
     * onAbsorb
     *
     * @param edgeEffect Object
     * @param velocity   int
     * @return boolean
     */
    public static boolean onAbsorb(Object edgeEffect, int velocity) {
        ((EdgeEffect) edgeEffect).onAbsorb(velocity);
        return true;
    }

    /**
     * draw
     *
     * @param edgeEffect Object
     * @param canvas     Canvas
     * @return boolean
     */
    public static boolean draw(Object edgeEffect, Canvas canvas) {
        return ((EdgeEffect) edgeEffect).draw(canvas);
    }
}
