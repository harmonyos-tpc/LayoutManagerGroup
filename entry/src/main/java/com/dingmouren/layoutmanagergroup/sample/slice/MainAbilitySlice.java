/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.dingmouren.layoutmanagergroup.sample.slice;

import com.dingmouren.layoutmanagergroup.echelon.EchelonLayoutManager;
import com.dingmouren.layoutmanagergroup.sample.ResourceTable;
import com.dingmouren.layoutmanagergroup.sample.adapter.EchelonAdapter;
import com.openharmony.recyclercomponent.HarmonyConstant;
import com.ohos.RecyclerContainer;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;

/**
 * Function description
 * class MainAbilitySlice
 */
public class MainAbilitySlice extends AbilitySlice {
    private RecyclerContainer recyclerView;
    private DirectionalLayout root = null;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        if (findComponentById(ResourceTable.Id_root) instanceof DirectionalLayout) {
            root = (DirectionalLayout) findComponentById(ResourceTable.Id_root);
        }
        recyclerView = new RecyclerContainer(this);
        root.addComponent(recyclerView,
                new ComponentContainer.LayoutConfig(HarmonyConstant.getMaximumWindowMetrics(this).getWidth(),
                        HarmonyConstant.getMaximumWindowMetrics(this).getHeight()));
    }

    @Override
    public void onActive() {
        super.onActive();
        EchelonLayoutManager echelonLayoutManager = new EchelonLayoutManager(this);
        recyclerView.setLayoutManager(echelonLayoutManager);
        EchelonAdapter echelonAdapter = new EchelonAdapter();
        recyclerView.setAdapter(echelonAdapter);
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
